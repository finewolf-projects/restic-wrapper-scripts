#!/bin/bash
#shellcheck disable=SC2034

AWS_ACCESS_KEY_ID=EXAMPLE_FOR_WASABI
AWS_SECRET_ACCESS_KEY=EXAMPLE_FOR_WASABI

RESTIC_REPOSITORY="s3:https://s3.us-east-1.wasabisys.com/bucket-name"
RESTIC_PASSWORD="SuperLongEncryptionKeyHere"

RESTIC_ARGS_init=(
  "--repository-version" "stable"
)
RESTIC_ARGS_all=(
  "--compression" "auto"
)
