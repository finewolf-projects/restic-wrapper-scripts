#!/bin/bash
#shellcheck disable=SC2034

#shellcheck disable=SC1091
source "${RESTIC_PROFILE_DIR}/../shared.sh";

RESTIC_ARGS_backup=(
  "--tag=example-profile"
  "--files-from=${RESTIC_PROFILE_DIR}/files"
  "--exclude-file=${RESTIC_PROFILE_DIR}/exclusions"
)

RESTIC_ARGS_forget=(
  "--tag=example-profile"
  "--keep-last=2"
  "--keep-daily=14"
  "--keep-weekly=8"
  "--keep-monthly=12"
  "--keep-yearly=2"
)

RESTIC_ARGS_snapshots=(
  "--tag=example-profile"
)
