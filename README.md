# Restic Wrapper Scripts

## Requirements

You must have `restic` installed within your `$PATH`.

## Setting Global Parameters

You can configure global parameters in `./profiles/shared.sh`. See Restic documentation for supported variables. Make sure to
generate an appropriate password in this file.

## Creating a profile

1. Create a folder under `./profile`
1. Create a `restic-profile.sh` file underneath there.
1. As an example use:

    ```bash
    #!/bin/bash
    #shellcheck disable=SC2034

    #shellcheck disable=SC1091
    source "${RESTIC_PROFILE_DIR}/../shared.sh";

    RESTIC_ARGS_backup=(
      "--tag=example-profile"
      "--files-from=${RESTIC_PROFILE_DIR}/files"
      "--exclude-file=${RESTIC_PROFILE_DIR}/exclusions"
    )

    RESTIC_ARGS_forget=(
      "--tag=example-profile"
      "--keep-last=2"
      "--keep-daily=14"
      "--keep-weekly=8"
      "--keep-monthly=12"
      "--keep-yearly=2"
    )

    RESTIC_ARGS_snapshots=(
      "--tag=example-profile"
    )

    ```

1. Make sure to replace the `tag` parameters. You can override the parameters of any command by setting `RESTIC_ARGS_command-name`.


## Initing the repository

Simply call the wrapper script as such:

```bash
./restic-wrapper.sh --profile example-profile -- init
```

By default, the same repository is used for all profiles (and backup scopes are managed by tags), therefore you only need to run `init` once, with any profile.

## Running a backup under a profile

Simply call the wrapper script as such:

```bash
./restic-wrapper.sh --profile example-profile -- backup
```

